// todo module docs

const child_process = require('child_process');
const util = require('util');

// todo docs
module.exports.send_code = function (remote_name, button_text) {
    // todo implement
    const model = require('../../remotes/' + remote_name + '/lirc');
    const codename = model[button_text];
    if (typeof codename === 'undefined') {
        console.error('No code for text: %s', button_text);
    } else {
        // todo use a nodejs module for lirc?
        const lircd_socket = '/var/run/lirc/lircd';
        const command = util.format('irsend --device=%s SEND_ONCE %s %s', lircd_socket, remote_name, codename);
        console.log('About to run: %s', command);
        child_process.exec(command, function (err, stdout, stderr) {
            if (err) {
                console.error(err);
            }
            if (stdout) {
                console.log('Standard output for: %s:\n%s', command, stdout);
            }
            if (stderr) {
                console.log('Standard err for: %s:\n%s', command, stderr);
            }
        });
    }
};
