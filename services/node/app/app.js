// Require system modules.
const bodyParser = require('body-parser');
const express = require('express');
const logger = require('morgan');
const path = require('path');
const serveFavicon = require('serve-favicon');

// Require custom modules.
const index = require('./server/routes/index');

// Create a new Express application.
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'server/views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'client')));
app.use(express.static(path.join(__dirname, 'remotes')));
app.use(serveFavicon(path.join(__dirname, 'images/tv-2.png')));

app.use('/remotes', express.static(path.join(__dirname, 'src/remotes')));

app.use('/', index);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
