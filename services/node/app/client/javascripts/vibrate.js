if (!'vibrate' in navigator) {
    alert('Your browser does not support vibrations.');

} else {
    document.querySelectorAll("button").forEach((button) => {
        button.addEventListener('click', function () {
            navigator.vibrate(100);
        });
    });
}
